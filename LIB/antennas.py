﻿# -*- coding: utf-8 -*-
"""
This is the antennas module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
from importlib import reload
from inspect import getsourcefile   # !! inquiry of script location

import os

import numpy as np

# EXTEND PATH
# ..

# INTERNAL
import functions as func  # !! functional routines applicable to many cases
import constants as const  # !! functional routines applicable to many cases

# RELOAD
reload(func)
reload(const)

# ==============================================================================
# DEFINE MEASUREMENT QUANTITIES
# ==============================================================================

symbols = {\
    'H' : {'unit' : 'A_per_m', 'label' : 'A/m'},\
    'B' : {'unit' : 'T'},\
    't' : {'unit' : 's'},\
    'w' : {'unit' : 'Hz'},\
    'U' : {'unit' : 'V', 'label' : 'V'},\
    'E' : {'unit' : 'V/m', 'label' : 'V/m'},\
    'P' : {'unit' : 'W'},\
    'R' : {'unit' : 'Ohm', 'label' : r'$\Omega$'}\
}

units = {\
    'A/m' : {'symbolic' : 'H', 'string' : 'A_per_m'},\
    'T' : {'symbolic' : 'B'},\
    's' : {'symbolic' : 't'},\
    'Hz' : {'symbolic' : 'w'},\
    'V' : {'symbolic' : 'U'},\
    'W' : {'symbolic' : 'P'},\
    'Ohm' : {'symbolic' : 'R'}\
}

sub_units = {\
    'p' : {'factor' : 1.e-12},\
    'n' : {'factor' : 1.e-9},\
    'u' : {'factor' : 1.e-6},\
    'm' : {'factor' : 1.e-3},\
    'k' : {'factor' : 1.e3},\
    'M' : {'factor' : 1.e6},\
    'G' : {'factor' : 1.e9}\
}

math_operators = {\
    'log10' : 'np.log10',
    'sig' : 'np.sign',
    'if' : 'np.where',
    'abs' : 'np.abs',
    ';' : ','
}

# ==============================================================================
# DEFINE FUNCTIONS
# ==============================================================================

def functionparser(function_string):
    global symbols
    # get both hand sides
    function_string = function_string.replace("'","").replace('"','').replace("\\","/")
    equation = function_string.split('=')
    # get lefthandside unit
    for quantity in symbols.keys():
        if quantity+'(' in equation[0]:
            break
        else:
            quantity = None
    # get lefthandside dependency
    for variable in symbols.keys():
        if '('+variable+')' in equation[0]:
            break
        else:
            variable = None
    # get right hand side parameters
    parameters = []
    for letter in equation[1]:
        if letter in symbols:
            parameters.append(letter)
    # write function header
    function = 'def antennafunction(**kwargs):\n'
    # prepare dynamic allocation of parameters to input
    for symbol in symbols.keys():
        if symbol in equation[1]:
            function = function + '    '+symbol+'=kwargs.get("'+symbol+'",None)\n'
    # react to SI-unit factors inside the function
    known_units = {}
    for unit in units.keys():
        known_units[unit] = 1.
        for sub in sub_units.keys():
            known_units[sub+unit] = sub_units[sub]['factor']
    for unit in known_units.keys():
        if '['+unit+']' in equation[1]:
            equation[1] = equation[1].replace('['+unit+']','*'+str(known_units[unit]))
    # replace math operations
    for operator in math_operators.keys():
        if operator in equation[1]:
            equation[1] = equation[1].replace(operator,math_operators[operator])
    # write main function
    function = function + '    return ' + equation[1] +'\n'
    # execute fuction statement and add its variable to the global scope such as it can be called by return
    exec(function, globals())
    return {'function':antennafunction,'quantity':quantity,'variable':variable,'parameters':parameters}
    
def signal_to_physical(**kwargs):
    function_string = kwargs.get('antennafactor',None)
    if '=' in function_string :
        # match input signal and antenna function variable
        try:
            # parse function string to antenna function
            result = functionparser(function_string)
            # find out if required parameters are given
            parameter_dict = {}
            for parameter in result['parameters']:
                if parameter not in kwargs:
                    return None
                else:
                    parameter_dict[parameter] = kwargs.get(parameter,None)
            # calculate physical information from signal
            physical =  result["function"](**parameter_dict)
        except:
            print("Trouble with given function for antenna factor.")
            return None
    else:
        # load characteristics from file
        try:
            # todo
            hardexit = 1/0
        except:
            print("Trouble with given function for antenna factor.")
            return None
    # return
    print(result)
    return {'quantity':result["quantity"],'variable':result["variable"], 'result': physical}
    