﻿# -*- coding: utf-8 -*-
"""
This is the exporter module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
from importlib import reload

import os
import sys
import errno

# EXTEND PATH
# ..

# INTERNAL
# ..

# RELOAD
# ..

# ==============================================================================
# CONSTANTS USED IN FUNCTIONS
# ==============================================================================

# ==============================================================================
# FUNCTIONS TO MANAGE DATA EXPORT
# ==============================================================================
def check_isdir(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
    return True
    
def dict_to_inputfile(path,lookup_dict,**kwargs):
    """
    EXPORT TO INPUT CSV FILE FROM INPUT DICT
    PATH            :: EXPORT PATH
    LOOKUP_DICT     :: TAKE DATA FROM HERE
    """
    # PARSE GLOBALS
    glob_dict = globals()
    # STRING MODIFICATION FOR SAFETY
    try:
        path = path.replace('/',os.path.sep)
    except:
        func.MESSAGE_PRINT(glob_dict['__file__'],"path does not look like a string")
    # CHECK AND GO
    path_to_file = path.split(os.path.sep,-1)[0]
    if check_isdir(path_to_file):
        print('\nPARSE TO INPUTFILE')
        # assemble lines
        output_list = ['key,value,help']
        for key in lookup_dict.keys():
            if 'help' not in key:
               shrink_help_message = (lookup_dict[key+"_help"].split("of type ")[1]).split(" .")[0]
               output_list.append(key+","+str(lookup_dict[key])+","+shrink_help_message.replace(" ","_"))
        # stitch lines
        output_string = "\n".join(output_list)
        # write out
        with open(path,"w") as f:
            f.write(output_string)
            return True
    # DEFAULT
    return False
    
def tkinter_to_inputfile(path,lookup_tkinter_object_dict,lookup_dict):
    """
    EXPORT TKINTER FIELD OBJECT CONTENT TO INPUT CSV FILE FROM INPUT DICT
    PATH            :: EXPORT PATH
    lookup_tkinter_object_dict :: TKINTER FIELD LIBRARY
    LOOKUP_DICT     :: OVERWRITE DATA IN HERE
    """
    for key in lookup_tkinter_object_dict.keys():
        if 'value' in key:
            key_base = key.split("_value")[0]
            try:
                exec("lookup_dict['"+key_base+"'] = float(lookup_tkinter_object_dict['"+key+"'].get())")
            except:
                exec("lookup_dict['"+key_base+"'] = lookup_tkinter_object_dict['"+key+"'].get()")
    return dict_to_inputfile(path,lookup_dict)