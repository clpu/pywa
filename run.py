# -*- coding: utf-8 -*-
"""
USER INFORMATION TO BE FOUND IN THE FILE README.MD

project             pythonic waveform analyser
acronym             pyWA
created on          2021-10-19:10:00

@author             Micha (MEmx), CLPU, Villamayor, Spain
@moderator          Micha (MEmx), CLPU, Villamayor, Spain
@updator            Micha (MEmx), CLPU, Villamayor, Spain
            
@contact            mehret@clpu.es

interpreter         python > 3.8
version control     git

requires explicitely {
  - opencv
  - tifffile
  - matplotlib
  - scipy
  - scikit-image
  - imutils
  - tkinter
}
  
which includes implicitely {
  - numpy
  - inspect
  - importlib
  - os
  - sys
  - time
}

input file {
  - CSV COLUMNS :: KEY, VALUE, HELP
  - CSV ROWS    :: ASSIGNMENTS {KEY => VALUE}
}

flags {
  - 
}

execute command: python run.py *args[input_file.csv]

"""
# ==============================================================================
# IMPORT PYTHON MODULES
# ==============================================================================
# EXTERNAL
from importlib import reload        # !! management of self-made modules
from inspect import getsourcefile   # !! inquiry of script location

import os                           # !! methods of operating system
import sys                          # !! executions by operating system
import time                         # !! time and date

import numpy as np                  # !! relatively fast array operations
import cv2                          # !! operations on images
import imutils                      # !! math on images

import readchar                     # !! allows to read key input of stdin

import tkinter
from tkinter import *
from tkinter import ttk
import tkinter as tk 

import matplotlib.pyplot as plt
import random

import matplotlib.image as mpimg
import scipy.fft
import scipy.interpolate
import PIL.Image, PIL.ImageTk
 
import glob

import math

from skimage import util

from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
import numpy as np

# EXTEND PATH
root = os.path.dirname(os.path.abspath(getsourcefile(lambda:0))) # !! get environment
sys.path.append(os.path.abspath(root+os.path.sep+'LIB'))         # !! add library directory
sys.path.append(os.path.abspath(root+os.path.sep+'SCR'))         # !! add script directory

# INTERNAL
import constants as const       # !! global constants from LIB/constants.py
import formats as formats       # !! global constants from LIB/constants.py
import functions as func        # !! global formats from SCR/functions.py
import loader as load           # !! routines fetching input data from SCR/loader.py
import exporter as export       # !! routines writing out data from SCR/exporter.py

import image as image           # !! treatment of graphical data from SCR/image.py

import antennas as antennas     # !! treatment of antenna data from SCR/antennas.py

# RELOAD
reload(const)
reload(func)
reload(load)
reload(export)
reload(image)

# STYLES
plt.style.use(root+os.path.sep+'LIB'+os.path.sep+'plots.mplstyle')

# ==============================================================================
# SET UP RUNTIME ENVIRONMENT
# ==============================================================================
# SEARCH FOR COMMAND LINE INPUT
args = func.COMMAND_LINE(['absolute_path_to_inputfile', \
                          'flags'])

# TAKE TIME
debut = time.time()

# PROMPT GREETINGS
print('\nWELCOME TO WA')

# CLEARIFY RUN
script_path,work_path = func.ENVIRONMENT(root = root)

# SET PARAMETERS


# ROOM FOR TESTS
# ..

# ==============================================================================
# LOAD VARIABLES
# ==============================================================================
# PARSE INPUT FILES
if args[1] == None:
    input_dict = load.parse_inputfile(const.ns_inputfile,[script_path,work_path])
else:
    input_dict = load.parse_inputfile(args[1],[script_path,work_path])

# ==============================================================================
# SET UP RUNTIME VARIABLES
# ==============================================================================
# GLOBALS
# ..

# ENVIRONMENT
a = root

# TEST CASE
# ..

# SET RUNTIME VARIABLE DICTIONARY
runtime_vars = {}

# PARSE META INFO FOR WAVEFORM FILE
if "waveform_query" in input_dict.keys():
    runtime_vars['resolve_waveform_query'] = load.parse_inputfile(input_dict['waveform_query'],[script_path,work_path])

# PARSE META INFO FOR SIGNAL INTERPRETATIONs
if "signal_query" in input_dict.keys():
    if input_dict['signal_query'].lower() != 'none':
        runtime_vars['resolve_signal_query'] = load.parse_inputfile(input_dict['signal_query'],[script_path,work_path])
    else:
        input_dict.pop('signal_query')

# GLOBAL VARIABLES
# ..

# RUNTIME PARSER
runtime_vars['working_dir'] = func.GIVE_DIRNAME(input_dict['working_dir'])
runtime_vars['output_dir'] = func.GIVE_DIRNAME(input_dict['output_dir'])

# ==============================================================================
# SET UP MODULES
# ==============================================================================
def load_waveform(*args,**kwargs):
    """
    LOAD WAVEFORM
    **kwargs:
    filename :: optional, dir of address
    m_GUI    :: optional, GUI if True, CMD else
    m_path   :: optional, base dir for file look-up,
                 important if m_GUI is set True

    """
    # PARSE GLOBALS
    global input_dict, runtime_vars
    glob_dict = globals()
    
    # INQUIRE OPTIONAL ARGUMENTS
    # modes of operation with defaults
    m_GUI = kwargs.get('m_GUI', False)
    m_path = kwargs.get('m_path', None)
    try:
        m_scope_channel = kwargs.get('m_scope_channel', input_dict['channel'])
    except:
        m_scope_channel = kwargs.get('m_scope_channel', None)
    m_antenna = kwargs.get('m_antenna', None)
    m_renormalization  = kwargs.get('m_renormalization', None)
    # exits query?
    filename = kwargs.get('filename', None)
    # SET UP VARIABLES
    feedback_message = None
    # SET UP OUTPUT DICTIONARY
    output_dict = {'filename' : filename, 'header': None, 'waveform' : None, 'renormalization' : None, 'message' : feedback_message}
    # GET FILE LOCATION
    if not isinstance(m_path, str):
        m_path = os.path.expanduser('~')
    if not isinstance(filename, str):
        if m_GUI == True:
            filename =  tk.filedialog.askopenfilename(\
                            initialdir = runtime_vars['working_dir'],\
                            title = "Select a File",\
                        )
        else:
            filename = input(" > type dir/filename.type and confirm with ENTER: ")
    # FEEDBACK
    func.MESSAGE_PRINT(glob_dict['__file__'],"target file is "+filename)
    # LOAD FILE
    if 'resolve_waveform_query' in runtime_vars.keys() and m_scope_channel is not None:
        # sort out bin file
        file_intelligence = func.DECOMPOSE_FILENAME(filename)
        if file_intelligence['extension'] == ".bin":
            if runtime_vars['resolve_waveform_query']['extra_ending_data'] != func.DECOMPOSE_FILENAME(file_intelligence['name'])['extension']:
                tmp_message = "tried to open meta file: failed"
                func.MESSAGE_PRINT(glob_dict['__file__'],tmp_message)
                output_dict['message'] = tmp_message
                return output_dict
            
            from RSRTxReadBin import RTxReadBin
            # load all
            y, x, S = RTxReadBin(filename)
            vertical = np.transpose(y[:,0,:]) 
            #print(S['MultiChannelVerticalOffset'])
            horizontal = x
            header = S
            # get rid of extra string waste
            for key in header.keys():
                if isinstance(header[key],str):
                    header[key] = header[key].replace("eRS_UNIT_LEVEL_","")
                    header[key] = header[key].replace("eRS_DECIMATION_MODE_","")
                    header[key] = header[key].replace("eRS_TRACE_ARITHMETICS_","")
                    header[key] = header[key].replace("eRS_TRACE_TYPE_","")
                    header[key] = header[key].replace("eRS_ONOFF_","")
                    header[key] = header[key].replace("eRS_SIGNAL_VALUE_TYPE_","")
                    header[key] = header[key].replace("eRS_USE_INTERSAMPLE_OFFSET_","")
                    header[key] = header[key].replace("eRS_SIGNAL_SOURCE_","")
                    header[key] = header[key].replace("eRS_NUMERIC_FORMAT_","")
                    header[key] = header[key].replace("eRS_ENHANCEMENT_MODE_","")
                    header[key] = header[key].replace("eRS_INTERPOLATION_MODE_","")
                    header[key] = header[key].replace("eRS_MATH_SPECTRAL_WINDOW_TYPE_","")
                    header[key] = header[key].replace("eRS_MATH_SPECTRAL_GATE_RBW_COUPLING_","")
                    header[key] = header[key].replace("eRS_SIGNAL_FORMAT_","")
                    header[key] = header[key].replace("eRS_EXPORT_ORIGIN_","")
                    header[key] = header[key].replace("eRS_BYTE_ORDER_","")
                    header[key] = header[key].replace("eRS_LINLOG_","")
                    header[key] = header[key].replace("eRS_TDR_TDT_RESULT_SIGNAL_DOMAIN_","")
            # interpret columns
            N_col = 0 # as implicit default TODO generalize
            for i in range(100,0,-1):
                if str(i) in str(input_dict['channel']):
                    N_col = i - 1
            # get waveform
            waveform = np.zeros((horizontal.shape[0],2))
            waveform[:,0] = horizontal
            waveform[:,1] = vertical[N_col,:]  
        elif 'header_length' in runtime_vars['resolve_waveform_query'].keys() and \
          'one_file' in runtime_vars['resolve_waveform_query'].keys() : # load other waveform data
            if runtime_vars['resolve_waveform_query']['one_file'] == 'T': 
                # get data to variable
                waveform = np.genfromtxt(filename, dtype=float, delimiter=',', skip_header=int(runtime_vars['resolve_waveform_query']['header_length']), names=True)
                # secure data
                for data_column in waveform.dtype.names:
                    pos_inf_positions = np.where(waveform[data_column] == np.inf)
                    neg_inf_positions = np.where(waveform[data_column] == - np.inf)
                    mask = np.isfinite(waveform[data_column])
                    clip_positiveinf_floats_to = np.nanmax(waveform[data_column][mask])
                    clip_negativeinf_floats_to = np.nanmin(waveform[data_column][mask])
                    waveform[data_column][pos_inf_positions] = clip_positiveinf_floats_to
                    waveform[data_column][neg_inf_positions] = clip_negativeinf_floats_to
                # interpret columns
                waveform = waveform[[runtime_vars['resolve_waveform_query']['name_of_time_column'],m_scope_channel]]
                # parse header information
                header_matrix = np.genfromtxt(filename, dtype=str, delimiter='\n', max_rows=int(runtime_vars['resolve_waveform_query']['header_length']))
                header = {}
                for row in header_matrix:
                    try:
                        split = row.split(runtime_vars['resolve_waveform_query']['meta_delimiter'].replace('"','').replace("'",''))
                    except:
                        print(' > no delimiter in')
                        print(split)
                    if (isinstance(split,str) or len(split) == 1) and ';' in runtime_vars['resolve_waveform_query']['meta_delimiter']:
                        split = row.split(',')
                    try:
                        header[split[0].replace('"','').replace("'",'')] = split[1]
                    except:
                        print(' > no index in')
                        print(split)
            elif runtime_vars['resolve_waveform_query']['one_file'] == 'F':
                # get filename primitive
                filename_stub_ending_list = filename.rsplit(".",maxsplit=2)
                # check for data file, i.e. kick out meta file
                if runtime_vars['resolve_waveform_query']['extra_ending_data']+'.'+filename_stub_ending_list[-1] not in filename:
                    tmp_message = "tried to open meta file: failed"
                    func.MESSAGE_PRINT(glob_dict['__file__'],tmp_message)
                    output_dict['message'] = tmp_message
                    return output_dict
                # get data to variable
                waveform = np.genfromtxt(filename, dtype=float, delimiter=',', skip_header=int(runtime_vars['resolve_waveform_query']['header_length']))
                # secure data
                for data_column in range(np.size(waveform,0)):
                    pos_inf_positions = np.where(waveform[data_column] == np.inf)
                    neg_inf_positions = np.where(waveform[data_column] == - np.inf)
                    mask = np.isfinite(waveform[data_column])
                    clip_positiveinf_floats_to = np.nanmax(waveform[data_column][mask])
                    clip_negativeinf_floats_to = np.nanmin(waveform[data_column][mask])
                    waveform[data_column][pos_inf_positions] = clip_positiveinf_floats_to
                    waveform[data_column][neg_inf_positions] = clip_negativeinf_floats_to
                # interpret columns
                N_col = 0 # as implicit default TODO generalize
                for i in range(100,0,-1):
                    if str(i) in str(input_dict['channel']):
                        N_col = i - 1
                # parse header information
                header_matrix = np.genfromtxt(filename.replace(runtime_vars['resolve_waveform_query']['extra_ending_data'],""), dtype=str, delimiter='\n')
                header = {}
                for row in header_matrix:
                    try:
                        split = row.split(runtime_vars['resolve_waveform_query']['meta_delimiter'].replace('"','').replace("'",''))
                    except:
                        print(' > no delimiter in')
                        print(split)
                    if (isinstance(split,str) or len(split) == 1) and ';' in runtime_vars['resolve_waveform_query']['meta_delimiter']:
                        split = row.split(',')
                    try:
                        header[split[0].replace('"','').replace("'",'')] = split[1]
                    except:
                        print(' > no index in')
                        print(split)
                # construct time axis if necessary and merge with waveform, else slice waveform
                if runtime_vars['resolve_waveform_query']['name_of_time_column'] == 'None':
                    try:
                        time_steps = waveform[:,0].size
                    except:
                        time_steps = waveform.size
                    timeline = np.linspace(0.,time_steps * float(header[runtime_vars['resolve_waveform_query']['headerinfokey_sampleinterval'].replace('"','').replace("'",'')]),num = time_steps)
                    try:
                        waveform = np.transpose(np.array([timeline,waveform[:,N_col]]))
                    except:
                        waveform = np.transpose(np.array([timeline,waveform]))
                else:
                    try:
                        N_time = int(runtime_vars['resolve_waveform_query']['name_of_time_column'])
                    except:
                        waveform = None
                        header = None
                        tmp_message = "target file can not get a time column"
                        func.MESSAGE_PRINT(glob_dict['__file__'],tmp_message)
                        feedback_message = tmp_message
                    waveform = waveform[:, [N_time,N_col+1]]
            else:
                tmp_message = "target file can not be attributed to a known oscilloscope format"
                func.MESSAGE_PRINT(glob_dict['__file__'],tmp_message)
                output_dict['message'] = tmp_message
                return output_dict
        else:
            tmp_message = "essential information missing to allocate the meta data"
            func.MESSAGE_PRINT(glob_dict['__file__'],tmp_message)
            output_dict['message'] = tmp_message
            return output_dict
    else:
        tmp_message = "target file is not recognized as valid waveform"
        func.MESSAGE_PRINT(glob_dict['__file__'],tmp_message)
        output_dict['message'] = tmp_message
        return output_dict
    # structure the array
    try:
        from numpy.lib.recfunctions import unstructured_to_structured
        waveform = unstructured_to_structured(waveform, dtype=np.dtype([('time', np.float32), (str(input_dict['channel']), np.float32)]))
    except:
        tmp_message = "procedure is bound to failiure with no data types"
        func.MESSAGE_PRINT(glob_dict['__file__'],tmp_message)
        feedback_message = feedback_message + ' and ' + tmp_message
    # CORRECTION FOR A CONSTANT HARDWARE OR SOFTWARE ATTENUATION FACTOR
    if m_renormalization is None:
        if isinstance(waveform, np.ndarray):
            if isinstance(m_antenna,dict):
                renormalized = {} # !TODO
            else:
                renormalized = None
            
            if 'attenuation' in input_dict.keys():
                renormalized = 10.**(-input_dict['attenuation']/20.)
        else:
            renormalized = None
    else:
        renormalized = m_renormalization
    # RETURN
    return {'filename' : filename, 'header': header, 'waveform' : waveform, 'renormalization' : renormalized, 'message' : feedback_message}

# ==============================================================================
# SET UP FUNCTIONS
# ==============================================================================
# ..

# ==============================================================================
# SET UP GUI
# ============================================================================== 
def interactive_input(**kwargs):
    """
    GUI SETUP FOR INPUT PARAMETERS
    **kwargs:
    anyfile  :: optional, bool to get remote input or work with default
    filename :: optional, dir of input file, important if anyfile is set True
    prompts a window with a menue
    - save     :: save fields to input file
    - apply    :: go to run with info in fields
    """
    # GLOABALS I/O
    global input_dict
    global runtime_vars
    global const
    
    # PARAMETERS
    def exit_function():
        return True
    exit_call = kwargs.get('exit_call',exit_function)
    
    # INQUIRE OPTIONAL ARGUMENTS
    # filename for display input
    anyfile  = kwargs.get('anyfile', False)
    filename = kwargs.get('filename', const.ns_inputfile)
    #global runtime_vars
    
    # FIND RUNTIME ENVIRONMENT
    for key in runtime_vars.keys():
        if key in input_dict.keys():
            input_dict[key] = runtime_vars[key]
    
    # GUI INI
    # initiate the input window 
    inputwin = tk.Tk()
    this_is_the_lastly_opened_input_window = True
    
    # set window title 
    inputwin.title("INPUT MENU")
       
    # set window size via trim("int(width) x int(height) + int(xposition) + int(yposition)")
    wxh = func.screensize()
    screen_width = int(wxh.split('x')[0])
    screen_height = int(wxh.split('x')[1])
    scale = 3
    geometry_string = str(int(screen_width/scale))+"x"+str(int(screen_height/scale))+\
                       "+"+str(int(screen_width/3))+"+"+str(int(screen_height/3))
    inputwin.geometry(geometry_string)
       
    # set window background
    inputwin.config(background = "black")
    
    # GUI SCROLLBAR
    # create main frame
    main_frame = Frame(inputwin)
    main_frame.pack(fill=BOTH, expand = 1)
    # create a canvas, the only native thing with a scrollbar
    main_canvas = Canvas(main_frame)
    main_canvas.pack(side=LEFT, fill=BOTH,expand=1)
    # ad a scrollbar to frame, relate it to canvas
    scrollbar = ttk.Scrollbar(main_frame, orient=VERTICAL, command=main_canvas.yview)
    scrollbar.pack(side=RIGHT, fill=Y)
    # configure the canvas
    main_canvas.configure(yscrollcommand = scrollbar.set, background = "black")
    main_canvas.bind('<Configure>', lambda e: main_canvas.configure(scrollregion = main_canvas.bbox("all")))
    # create frame inside canvas
    slave_frame = Frame(main_canvas)
    slave_frame.config(background = "black")
    # add frame to window in canvas
    main_canvas.create_window((0,0), window = slave_frame, anchor="nw")
    
    # GUI MAIN APP
    
    # manifest logo
    background_image = image.IMREAD(root+os.path.sep+"MEDIA"+os.path.sep+"IMG"+os.path.sep+"CLPU_SF.png")
    bg_width = background_image.shape[1]
    bg_height = background_image.shape[0]
    bg_proportion = 3
    
    bg_scale_width = bg_width / (screen_width/scale) * bg_proportion
    bg_scale_height = bg_height / (screen_height/scale) * bg_proportion
    bg_scale = int(max(bg_scale_height,bg_scale_width))+1 # .. '+1' as primitive floor operation
    background_image = background_image[::bg_scale,::bg_scale,:]
    
    background_image = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(background_image))
    canvas = tk.Canvas(slave_frame,width = bg_width/bg_scale, height = bg_height/bg_scale)
    canvas.create_image(0,0,image=background_image, anchor="nw")
    
    # create labels
    label_title =   Label(\
                      slave_frame,\
                      text = "IOSSR", \
                      font=('Arial 21 bold'),\
                      width = 6, height = 2,\
                      fg = "lightgreen", \
                      background="black"\
                    )
    label_coucou =  Label(\
                      slave_frame,\
                      text = "Adjustments to input parameters:", \
                      font=('Arial 12 bold'),\
                      width = 30, height = 2,\
                      fg = "lightgreen", \
                      background="black"\
                    )
                    
    # create input fields
    rownumber = 6
    field_dict = {}
    for key in input_dict.keys():
        if 'help' not in key:
            # label for row
            Label(slave_frame,\
              text=key+' :: ',\
              font=('Arial 10 bold'),\
              fg = "lightgreen", \
              background="black"\
            ).grid(column=1, row=rownumber)
            # input field for row
            exec("field_dict['"+key+"_value'] = tk.StringVar(slave_frame, name=key, value=str(input_dict['"+key+"']))")
            exec("field_dict['"+key+"_entry'] = tk.Entry(slave_frame,"+\
                                    "textvariable= field_dict['"+key+"_value'],"+\
                                    "font=('Arial 10')"+\
                                  ").grid(column=2, row=rownumber, sticky='news')")
            # button for row
            exec("Button(slave_frame,"+\
              "text='Help', "+\
              "font=('Arial 10'),"+\
              "command=lambda: tk.messagebox.showwarning('Help', str(input_dict['"+key+"_help']))"+\
            ").grid(column=3, row=rownumber)")
        
            # Close row
            rownumber = rownumber + 1
    
    # control option
    #for key in input_dict.keys():
    #    exec("print("+key+"_value.get())")
    
    # helper function: get path
    def get_path(title,**kwargs):
        global input_dict
        io = kwargs.get('io',None)
        if io == 'save':
            return tk.filedialog.asksaveasfilename(\
                            initialdir = runtime_vars['working_dir'],\
                            title = title,\
                            defaultextension=".txt"\
                        )
        elif io=='open':
            return tk.filedialog.askopenfilename(\
                            initialdir = runtime_vars['working_dir'],\
                            title = title,\
                            defaultextension=".txt"\
                        )
        else:
            return tk.simpledialog.askstring(\
                            title = title,\
                            prompt="path/to/dir/file.csv"
                        )
   
    # helper function: reload window with new input parameters
    def rewind_input_dict(file):
        global input_dict
        nonlocal this_is_the_lastly_opened_input_window
        input_dict = load.parse_inputfile(file,[])
        inputwin.destroy()
        this_is_the_lastly_opened_input_window = False
        interactive_input(runtime_vars = runtime_vars)
        return True
        
    # helper function: prepare and execute export of settings
    def execute_export(file,field_dict,input_dict):
        export.tkinter_to_inputfile(file,field_dict,input_dict)
        return True
    
    # create buttons
    button_load_any      =   Button(\
                               slave_frame,  \
                               text = "IMPORT PARAMETERS", \
                               font=('Arial 12 bold'),\
                               command = lambda: rewind_input_dict(get_path("Select source file",io="open"))\
                             )
    
    button_goto_save     =   Button(\
                               slave_frame,  \
                               text = "EXPORT TO FILE", \
                               font=('Arial 12 bold'),\
                               command = lambda: execute_export(get_path("Save as file",io="save"),field_dict,input_dict)\
                             )
    button_goto_main     =   Button(\
                               slave_frame,  \
                               text = "APPLY", \
                               font=('Arial 12 bold'),\
                               command = inputwin.destroy\
                             )  
    
    # positioning
    canvas.grid(column = 1, row = 1)
    label_title.grid(column = 2, row = 1)
    label_coucou.grid(column = 2, row = 2)
    
    button_load_any.grid(column =2, row= 3, sticky="news")
    button_goto_save.grid(column =2, row= 4, sticky="news")
    button_goto_main.grid(column =2, row= 5, sticky="news")
    
    # TODO add scrollbar
    
    # create window
    inputwin.mainloop()
    
    # read fields and parse them to dictionary
    for key in input_dict.keys():
        if 'help' not in key and this_is_the_lastly_opened_input_window:
            try:
                exec("input_dict['"+key+"'] = float(field_dict['"+key+"_value'].get())")
            except:
                exec("input_dict['"+key+"'] = field_dict['"+key+"_value'].get()")
    
    # write working directory in runtime parameters
    runtime_vars['working_dir'] = func.GIVE_DIRNAME(input_dict['working_dir'])
    runtime_vars['output_dir'] = func.GIVE_DIRNAME(input_dict['output_dir'])
    
    # exit
    return exit_call()

def interactive_start():
    """
    GUI FOR START DIALOGUE
    prompts a window with a menue
    - start    :: continue to the main programme
    - settings :: go to change of default settings
    """
    # globals 
    global root
    # set output flags
    goto = None
    # initiate the start-up window 
    startup = tk.Tk()
    
    # set window title 
    startup.title("START MENU")
       
    # set window size via trim("int(width) x int(height) + int(xposition) + int(yposition)")
    wxh = func.screensize()
    screen_width = int(wxh.split('x')[0])
    screen_height = int(wxh.split('x')[1])
    scale = 3
    geometry_string = str(int(screen_width/scale))+"x"+str(int(screen_height/scale))+\
                       "+"+str(int(screen_width/3))+"+"+str(int(screen_height/3))
    startup.geometry(geometry_string)
       
    # set window background
    startup.config(background = "black")
    
    # manifest logo
    background_image = image.IMREAD(root+os.path.sep+"MEDIA"+os.path.sep+"IMG"+os.path.sep+"CLPU_SF.png")
    bg_width = background_image.shape[1]
    bg_height = background_image.shape[0]
    bg_proportion = 3
    
    bg_scale_width = bg_width / (screen_width/scale) * bg_proportion
    bg_scale_height = bg_height / (screen_height/scale) * bg_proportion
    bg_scale = int(max(bg_scale_height,bg_scale_width))+1 # .. '+1' as primitive floor operation
    background_image = background_image[::bg_scale,::bg_scale,:]
    
    background_image = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(background_image))
    canvas = tk.Canvas(startup,width = bg_width/bg_scale, height = bg_height/bg_scale)
    canvas.create_image(0,0,image=background_image, anchor="nw")
       
    # create labels
    label_title =   Label(\
                      startup,\
                      text = "WA", \
                      font=('Arial 21 bold'),\
                      width = 6, height = 2,\
                      fg = "lightgreen", \
                      background="black"\
                    )
    label_coucou =  Label(\
                      startup,\
                      text = "The online analysis for waveforms!", \
                      font=('Arial 12 bold'),\
                      width = 30, height = 2,\
                      fg = "lightgreen", \
                      background="black"\
                    )

    # create buttons     
    def goto_settings():
        startup.destroy()
        interactive_input()
        return True
    
    button_goto_settings =   Button(\
                               startup,  \
                               text = "SETTINGS", \
                               font=('Arial 12 bold'),\
                               command = goto_settings\
                             )
     
    def goto_main():
        startup.destroy()
        interactive_runtime()
        return True
    
    button_goto_main     =   Button(\
                               startup,  \
                               text = "START", \
                               font=('Arial 12 bold'),\
                               command = goto_main\
                             )
    
    button_close_window  =   Button(\
                               startup,  \
                               text = "CLOSE", \
                               font=('Arial 12 bold'),\
                               command = lambda: exit()\
                             )  
    
    # positioning
    #canvas.place(x=0, y=0, relwidth=1, relheight=1)
    canvas.grid(column = 1, row = 1)
    label_title.grid(column = 2, row = 1)
    label_coucou.grid(column = 2, row = 2)

    button_goto_settings.grid(column =2, row= 3, sticky="news")
    button_goto_main.grid(column =2, row= 4, sticky="news")
    button_close_window.grid(column =2, row= 5, sticky="news")
    
    
    # create window
    startup.mainloop()

    return True
    
def interactive_runtime(*args,**kwargs):
    # here, doing nothing yields the goal
    return True
    
def select_batch(**kwargs):
    """
    DIALOGUE FOR INPUT FILE SELECTION WITH FILE DIALOGUE
    **kwargs:
    m_GUI      :: use GUI or terminal input
    prompts a window with a menue
    - save     :: save fields to input file
    - apply    :: go to run with info in fields
    """
    # GLOABALS I/O
    # none
    
    # INQUIRE OPTIONAL ARGUMENTS
    m_GUI  = kwargs.get('m_GUI', False)
    
    # DIALOGUE INIT
    if not m_GUI:
        filenames = input(" > type dir/filename.type dir/filename2.type and confirm with ENTER: ")
        filenames = filenames.split(' ')
    else:
        filenames = list(tk.filedialog.askopenfilenames(title='Select Batch'))
    
    # OUT
    return filenames

# ==============================================================================
# START RUN
# ==============================================================================   

def explorador_referencia(**kwargs):
    # GLOBALS
    glob_dict = globals()
    
    # EXPECTED
    global runtime_vars
    
    # RUN
    # center coordinates
    reference_waveform = load_waveform(m_GUI=True, runtime_vars= runtime_vars)
    
    if isinstance(reference_waveform['waveform'],type(None)) or isinstance(reference_waveform['header'],type(None)):
        return False
    
    # .. gather meta data and resolve namespace
    namelist = reference_waveform['waveform'].dtype.names
    data_column = namelist[1] # presume second column to be the signal apmlitude
    
    # corrections on the detector (recorder) side
    if reference_waveform['renormalization'] is not None:
        correctedsignal = reference_waveform["waveform"]
        correctedsignal[data_column] = reference_waveform["waveform"][data_column] * reference_waveform['renormalization']
    else:
        correctedsignal = {}
        correctedsignal[data_column] = reference_waveform["waveform"]
    
    # calculate physical quantity from signal (receiver side)
    if 'resolve_signal_query' in runtime_vars.keys():
        if runtime_vars['resolve_waveform_query']['headerinfokey_verticalunit'].replace('"','').replace("'","") in reference_waveform['header'].keys():
            verticalunit = reference_waveform['header'][runtime_vars['resolve_waveform_query']['headerinfokey_verticalunit'].replace('"','').replace("'","")]
            # differentiate known types of measurement
            go_replace_signal = False
            if verticalunit == "V":
                parameter_dict = runtime_vars['resolve_signal_query']
                parameter_dict["U"] = correctedsignal[data_column]
                go_replace_signal = True
            if go_replace_signal:
                result = antennas.signal_to_physical(**parameter_dict)
                correctedsignal[data_column] = result['result']
            del verticalunit, parameter_dict, go_replace_signal, result
    
    # write out
    runtime_vars['renormalization'] = reference_waveform['renormalization']
    runtime_vars['reference_waveform'] = reference_waveform['waveform']
    runtime_vars['reference_correctedsignal'] = correctedsignal
    
    return True


def explorador_archivos(**kwargs): 
    # GLOBALS
    glob_dict = globals()
    global root
    
    # EXPECTED
    global runtime_vars
    global input_dict
    
    # OPTIONS
    data_file = kwargs.get('data_file',None)
    silent_run = kwargs.get('silent_run',False)
    plotting = kwargs.get('plotting',True)
    matplotlib_canvas = kwargs.get('plt_figure',None)
    matplotlib_axs = kwargs.get('plt_axs',None)
    
    # OUTPUT
    explored = {'timedomain': None,        'signal_data': None,    'frequencydomain': None,\
                'FFT_amplitudes': None,    'FFT_amplitudes': None, 'FFT_amplitudes': None,\
                'valid_frequencies': None, 'is_set_renorm': None,  'is_set_filteredFFT': None}

    # RUN
    if data_file is None:
        input_stream = load_waveform(m_GUI=True,m_renormalization=runtime_vars['renormalization'], runtime_vars= runtime_vars)
    else:
        input_stream = load_waveform(filename=data_file,m_renormalization=runtime_vars['renormalization'], runtime_vars= runtime_vars)
    #print(input_stream['message'])
    
    if isinstance(input_stream['waveform'],type(None)) or isinstance(input_stream['header'],type(None)):
        return None
    
    # MAIN
    print("\nPROCESS DATA")
    
    ###
    # gather meta data and resolve namespace
    namelist = input_stream["waveform"].dtype.names
    
    time_column = namelist[0] # presume first column to be the time information, or take given name:
    if 'resolve_waveform_query' in runtime_vars.keys():
        if 'name_of_time_column' in runtime_vars['resolve_waveform_query'].keys():
            if "none" not in runtime_vars['resolve_waveform_query']['name_of_time_column'].lower():
                time_column = runtime_vars['resolve_waveform_query']['name_of_time_column']
            else:
                time_column = 'time'
    print(" > TIME COLUMN IS "+time_column)
    
    is_set_timestep = False
    if 'resolve_waveform_query' in runtime_vars.keys():
        if 'headerinfokey_sampleinterval' in runtime_vars['resolve_waveform_query'].keys():
            if runtime_vars['resolve_waveform_query']['headerinfokey_sampleinterval'].replace('"','').replace("'","") in input_stream['header'].keys():
                timestep = float(input_stream['header'][runtime_vars['resolve_waveform_query']['headerinfokey_sampleinterval'].replace('"','').replace("'","")])
                is_set_timestep = True
    if not is_set_timestep: # TODO remove complicated construction of time column above and use this
        timestep = (correctedsignal[time_column][-1]-correctedsignal[time_column][0])/(samples-1)
    del is_set_timestep
    print("   TIMESTEP : "+str(timestep))
    
    data_column = namelist[1] # presume second column to be the signal apmlitude
    print(" > DATA COLUMN IS "+data_column)
    
    ###
    # Create signal and spectrum (start from recorder side)
    # (A) account for attenuation on channels
    if input_stream['renormalization'] is not None:
        print(" > APPLY RENORMALIZATION")
        correctedsignal = input_stream["waveform"]
        correctedsignal[data_column] = input_stream["waveform"][data_column] * input_stream['renormalization']  
    else:
        print(" > NO RENORMALIZATION")
        correctedsignal = {}
        correctedsignal[data_column] = input_stream["waveform"]
    
    # (B) account for cables and transmission line
    # correctedsignal = correctedsignal[::10]
    
    # .. adjust timeframe
    if 'resolve_waveform_query' in runtime_vars.keys():
        if 'headerinfokey_delay' in runtime_vars['resolve_waveform_query'].keys():
            if runtime_vars['resolve_waveform_query']['headerinfokey_delay'].replace('"','').replace("'","") in input_stream['header'].keys():
                print(" > ADJUST TIME BY TRIGGER DELAY")
                correctedsignal[time_column] = correctedsignal[time_column] - float(input_stream['header'][runtime_vars['resolve_waveform_query']['headerinfokey_delay'].replace('"','').replace("'","")])
    
    # (C) prepare to account for receiver attenuation
    is_set_renorm = False
    if 'resolve_signal_query' in runtime_vars.keys():
        # concider antenna factor that translates from measured units to field units
        if runtime_vars['resolve_waveform_query']['headerinfokey_verticalunit'].replace('"','').replace("'","") in input_stream['header'].keys():
            verticalunit = input_stream['header'][runtime_vars['resolve_waveform_query']['headerinfokey_verticalunit'].replace('"','').replace("'","")]
            print(" > VERTICAL UNIT IS ["+verticalunit+"]")
            # differentiate known types of measurement
            go_replace_signal = False
            if verticalunit == "V":
                go_replace_signal = True
            del verticalunit
        # prepare frequency response if available (consider antenna gain), in order to go back, from measurement to signal
        if 'frequencyresponse' in runtime_vars['resolve_signal_query'].keys():
            print(" > PREPARE DATA CORRECTION IN FREQUENCY DOMAIN (ANTENNA CORRECTION)")
            f_response_data = np.genfromtxt(runtime_vars['resolve_signal_query']['frequencyresponse'], dtype=float, delimiter=',', names=True)
            if 'reference_dBA_per_m' in f_response_data.dtype.names and 'antenna_dBA_per_m' in f_response_data.dtype.names:
                f_renorm = scipy.interpolate.interp1d(f_response_data['f_MHz']*const.MHz_to_Hz, 10.**((f_response_data['reference_dBA_per_m']-f_response_data['antenna_dBA_per_m'])/20.),fill_value='extrapolate')
                is_set_renorm = True
            elif 'antenna_dBi' in f_response_data.dtype.names:
                f_renorm = scipy.interpolate.interp1d(f_response_data['f_MHz']*const.MHz_to_Hz, 10.**((f_response_data['antenna_dBi'])/20.),fill_value='extrapolate')
                is_set_renorm = True
            elif 'antenna_factor' in f_response_data.dtype.names:
                f_renorm = scipy.interpolate.interp1d(f_response_data['f_MHz']*const.MHz_to_Hz, 10.**((f_response_data['antenna_factor'])/20.),fill_value='extrapolate')
                is_set_renorm = True
            else:
                tmp_message = "can not make sense out of given antenna factor file"
                func.MESSAGE_PRINT(glob_dict['__file__'],tmp_message)
                is_set_renorm = False    
    
    ###
    # Calculate physical quantity from signal
    # .. calculate FT
    print(" > FOURIER TRANSFORM")
    samples  = correctedsignal[data_column].size
    print("   SAMPLES  : "+str(samples))
    
    print('   TRANSFORM RAW..')
    signal_FFT         = scipy.fft.rfft(correctedsignal[data_column],workers = os.cpu_count())
    signal_frequencies = scipy.fft.rfftfreq(samples, d = timestep)
    #print(signal_FFT)
    print('   ..OK')
    
    valid_frequencies = np.where(signal_frequencies > 0.)
    
    if is_set_renorm: # finally do (C)
        print('   APPLY FREQUENCY DOMAIN CORRECTIONS')
        modified_frequencies = np.where(signal_frequencies < runtime_vars['resolve_signal_query']['bandwidth_up'])
        modified_frequencies = np.where(signal_frequencies[modified_frequencies] > runtime_vars['resolve_signal_query']['bandwidth_lo'])
        signal_FFT_renormalized = signal_FFT * f_renorm(signal_frequencies)
    else:
        print('   SKIP FREQUENCY DOMAIN CORRECTIONS')
        signal_FFT_renormalized = signal_FFT
        
    # .. retransform to time base
    correctedsignal[data_column] = scipy.fft.irfft(signal_FFT_renormalized, samples)
    
    # .. apply antenna factor for unit conversion
    measurement = {}
    if go_replace_signal:
        parameter_dict = runtime_vars['resolve_signal_query']
        parameter_dict["U"] = correctedsignal[data_column]
        print(" > OVERWRITE DATA COLUMN BY ANTENNA CALLIBRATION (UNIT CONVERSION)")
        result = antennas.signal_to_physical(**parameter_dict)
        measurement[data_column] = result['result']
        measurement[time_column] = correctedsignal[time_column]
        for argument in result.keys():
            if argument != 'result':
                runtime_vars[argument] = result[argument]
        del result, parameter_dict
    
        # fourier transform to final magnitude spectrum
        print('   TRANSFORM UNIT CONVERTED..')
        measurement_FFT_renormalized = scipy.fft.rfft(measurement[data_column],workers = os.cpu_count())
        #print(measurement_FFT_renormalized)
        print('   ..OK')
    else:
        measurement_FFT_renormalized = signal_FFT_renormalized
        measurement[data_column] = correctedsignal[data_column]
        measurement[time_column] = correctedsignal[time_column]
    
    # .. and of ref - TODO move up and in dict
    print('   REFERENCE TRANSFORM..')
    ref_FFT = scipy.fft.rfft(runtime_vars['reference_correctedsignal'][data_column],workers = os.cpu_count())
    ref_frequencies = signal_frequencies
    print('   ..OK')
    
    # .. and filter signal by bandwidth of circuit
    print(' > BANDWIDTH ANALYSIS')
    is_set_filteredFFT = False
    if 'resolve_waveform_query' in runtime_vars.keys():
        if 'headerinfokey_model' in runtime_vars['resolve_waveform_query'].keys():
            if runtime_vars['resolve_waveform_query']['headerinfokey_model'].replace('"','').replace("'","") in input_stream['header'].keys():
                # details of oscilloscope
                print('   LOAD OSCILLOSCOPE PARAMETERS')
                scope_model = input_stream['header'][runtime_vars['resolve_waveform_query']['headerinfokey_model'].replace('"','').replace("'","")]
                # TODO library of translations
                if scope_model == "5.0.1.0":
                    scope_model = "RTO64"
                # load infos
                scope_infos = load.parse_inputfile('LIB/oscilloscopes/'+scope_model+'.dat',[script_path,work_path],silent=True)
                # filter according to oscilloscope
                try:
                    bandwidth_up = scope_infos['bandwidth_up']
                    bandwidth_lo = scope_infos['bandwidth_lo']
                    print('   APPLY BANDWIDTH OF '+scope_model)
                    is_set_filteredFFT = True
                except:
                    print("   DO NOT FIND BANDWIDTH INFORMATION FOR "+scope_model)
                # filter according to antenna
                try:
                    if not is_set_filteredFFT:
                        bandwidth_up = runtime_vars['resolve_signal_query']['bandwidth_up']
                        bandwidth_lo = runtime_vars['resolve_signal_query']['bandwidth_lo']
                        print('   APPLY BANDWIDTH OF '+runtime_vars['resolve_signal_query']['model'])
                        is_set_filteredFFT = True
                    else:
                        bandwidth_up = min(bandwidth_up,runtime_vars['resolve_signal_query']['bandwidth_up'])
                        bandwidth_lo = max(bandwidth_lo,runtime_vars['resolve_signal_query']['bandwidth_lo'])
                        print('   TAKE INTO ACCOUNT BANDWIDTH OF '+runtime_vars['resolve_signal_query']['model'])
                except:
                    print("   DO NOT FIND FURTHER BANDWIDTH INFORMATION")
                # do filtering    
                if is_set_filteredFFT:
                    print('   LOWER CUT OF AT '+str(bandwidth_lo)+"Hz")
                    print('   UPPER CUT OF AT '+str(bandwidth_up)+"Hz")
                    print('   FILTER FOURIER TRANSFORM DATA..')
                    modified_frequencies = np.where(signal_frequencies < bandwidth_up)
                    modified_frequencies = np.where(signal_frequencies[modified_frequencies] > bandwidth_lo)
                    filtered_FFT = np.where(signal_frequencies > bandwidth_up,0.,measurement_FFT_renormalized)
                    filtered_FFT = np.where(signal_frequencies < bandwidth_lo,0.,filtered_FFT)

                    print('   ..OK')
                    print('   RE-TRANSFORM FILTERED SIGNAL..')
                    measurement[data_column] = scipy.fft.irfft(filtered_FFT, samples)
                    
                    print('   ..OK')
                    
                    del filtered_FFT

    
    # .. and chop signal to slices for streaked frequency view (Spectogram)
    #    see: https://www.oreilly.com/library/view/elegant-scipy/9781491922927/ch04.html
    print(' > CREATE SPECTROGRAM')
    slices = util.view_as_windows(measurement[data_column], window_shape=(2**int(input_dict['windowing']),), step=int(2**int(input_dict['windowing'])/10))
    win = np.hanning(2**int(input_dict['windowing']) + 1)[:-1]
    slices = slices * win
    slices = slices.T
    spectrum = np.fft.fft(slices, axis=0)[:2**int(input_dict['windowing']) // 2 + 1:-1]
    spectrum = np.abs(spectrum)
    S = 20 * np.log10(spectrum / np.max(spectrum))
    
    # storage section
    explored['timedomain'] = correctedsignal[time_column]
    explored['signal_data'] = correctedsignal[data_column]
    
    explored['corrected_data'] = measurement[data_column]
    
    explored['frequencydomain'] = signal_frequencies
    explored['FFT_amplitudes'] = signal_FFT
    
    explored['valid_frequencies'] = valid_frequencies
    explored['is_set_renorm'] = is_set_renorm
    explored['is_set_filteredFFT'] = is_set_filteredFFT
    
    if is_set_renorm:
        explored['signal_cleaned_data'] = measurement[data_column]
        explored['frequencydomain_cleaned'] = signal_frequencies
        explored['FFT_amplitudes_cleaned'] = measurement_FFT_renormalized
        explored['modified_frequencies'] = modified_frequencies
    
    # plotter section
    
    if plotting:
        print(' > PLOT')
        if matplotlib_canvas is None or True:
            plt.ioff()
            fig = plt.figure(figsize=(15,10), dpi=100)
            gs = fig.add_gridspec(2,2) #(3,2)
            ax0 = fig.add_subplot(gs[1, 0])
            ax3 = fig.add_subplot(gs[0, 0])
            ax1 = fig.add_subplot(gs[0, 1])
            ax2 = fig.add_subplot(gs[1, 1])
            print('   CANVAS OK')
        else:
            fig = matplotlib_canvas
            ax0,ax1,ax2 = matplotlib_axs
            ax0.clear()
            ax1.clear()
            ax2.clear()
            ax3.clear()
            print('   CANVAS PREPARED')
            
        # .. helper functions
        #    https://stackoverflow.com/a/14314054
        def moving_average(a, n=3) :
            ret = np.cumsum(a, dtype=float)
            ret[n:] = ret[n:] - ret[:-n]
            return ret[n - 1:] / n
            
        # .. plots in function calls for later onclick conpatibility
        def plot_ax0():
            nonlocal ax0
            nonlocal data_column
            nonlocal time_column
            # plot signal recorded with device
            p0 = ax0.plot(const.s_to_ns*correctedsignal[time_column],input_stream["waveform"][data_column], label="Signal", color = formats.color[0])
            ax0.set_ylabel('Amplitude (V)')
            
            ax0.yaxis.label.set_color(p0[0].get_color())
            ax0.tick_params(axis='y', colors=p0[0].get_color())
            
            spacing = 0.15
            
            # plot signal after eventual corrections (antenna factor, transmission line, attenuations)
            ax0b = ax0.twinx()
            ax0b.spines.right.set_position(("axes", 1.+0*spacing))
            
            if is_set_filteredFFT or is_set_renorm:
                plotlabel_add = "\n(w-domain corrected)"
            else:
                plotlabel_add = ""
            p0b = ax0b.plot(const.s_to_ns*correctedsignal[time_column],correctedsignal[data_column], label="Signal"+plotlabel_add, color = formats.color[1], linestyle = 'dashed')
            ax0b.set_ylabel('Amplitude (V)')
            
            ax0b.yaxis.label.set_color(p0b[0].get_color())
            ax0b.tick_params(axis='y', colors=p0b[0].get_color())
            
            # plot signal transformed to physical measurement
            ax0c = ax0.twinx()
            ax0c.spines.right.set_position(("axes", 1.+1*spacing))
            
            p0c = ax0c.plot(const.s_to_ns*measurement[time_column],measurement[data_column], label="Measurement"+plotlabel_add, color = formats.color[2])
            if 'quantity' in runtime_vars.keys():
                ax0c.set_ylabel(runtime_vars['quantity'] + ' ('+antennas.symbols[runtime_vars['quantity']]['label']+')')
            else:
                ax0c.set_ylabel('Amplitude (V)')
            
            ax0c.yaxis.label.set_color(p0c[0].get_color())
            ax0c.tick_params(axis='y', colors=p0c[0].get_color())
            
            # finishing
            ax0.legend(handles=[p0[0],p0b[0],p0c[0]])
            ax0.set_title('Data Waveform')
            ax0.set_xlabel('Time (ns)')
            
        print('   WAVEFORM')
        plot_ax0()
        
        def plot_ax1():
            nonlocal ax1
            averaging = 10
            ax1.scatter(signal_frequencies[valid_frequencies],np.abs(signal_FFT[valid_frequencies]),label='Signal', color = formats.lightcolor[0])
            ax1.plot(signal_frequencies[valid_frequencies][:-averaging+1],moving_average(np.abs(signal_FFT[valid_frequencies]),averaging), color = formats.color[0])
            if is_set_renorm:
                ax1.scatter(signal_frequencies[modified_frequencies],np.abs(measurement_FFT_renormalized[modified_frequencies]),label='Bandwidth and w corrected', color = formats.lightcolor[1])
                ax1.plot(signal_frequencies[modified_frequencies][:-averaging+1],moving_average(np.abs(measurement_FFT_renormalized[modified_frequencies]),averaging), color = formats.color[1])
                ax1.legend()
            ax1.set_title('Data Spectrum')
            ax1.set_xlabel('Frequency (Hz)')
            ax1.set_ylabel('Amplitude (Vs)')
        print('   SPECTRUM')
        plot_ax1()
        
        def plot_ax2():
            nonlocal ax2
            ax2.scatter(ref_frequencies[valid_frequencies],np.abs(ref_FFT[valid_frequencies]))
            ax2.set_title('Reference Spectrum')
            ax2.set_xlabel('Frequency (Hz)')
            ax2.set_ylabel('Amplitude (Vs)')
        print('   REFERENCE SPECTRUM')
        plot_ax2()
        
        def plot_ax3():
            nonlocal ax3
            nonlocal time_column
            ax3.imshow(S, origin='lower', cmap='viridis', extent=(const.s_to_ns*correctedsignal[time_column][0], const.s_to_ns*(correctedsignal[time_column][0] + timestep*samples), 0, 1 / timestep / 2))
            ax3.axis('tight')
            ax3.set_title('Data Spectogram')
            ax3.set_xlabel('Time (s)')
            ax3.set_ylabel('Frequency (Hz)')
        print('   SPECTROGRAM')
        plot_ax3()
        
        # .. style event management
        plt.tight_layout()
        def resize_here():
            plt.tight_layout()
        cre = fig.canvas.mpl_connect('resize_event', resize_here)
        
        # .. mouse klick event management
        coordinates = None
        def onclick(event):
            nonlocal coordinates
            nonlocal ax0
            nonlocal ax1
            nonlocal ax2
            nonlocal ax3
            if event.dblclick:
                # feedback
                coordinates = event.xdata, event.ydata
                print(" > click coordinates "+str(coordinates))
                # plotting
                if event.inaxes is not None:
                    # get position
                    axi = event.inaxes
                    # clear and replot
                    ax0.clear()
                    plot_ax0()
                    ax1.clear()
                    plot_ax1()
                    ax2.clear()
                    plot_ax2()
                    ax3.clear()
                    plot_ax3()
                    # auxiliary indication
                    if id(axi) == id(ax0) :
                        # helper text
                        # ..
                        # helper lines
                        ax3.axvline(x=event.xdata,color="darkorange", linestyle="--")
                    elif id(axi) == id(ax1) :
                        # helper text
                        # ..
                        # helper lines
                        ax2.axvline(x=event.xdata,color="darkorange", linestyle="--")
                        ax3.axhline(y=event.xdata,color="darkorange", linestyle="--")
                    elif id(axi) == id(ax2) :
                        # helper text
                        # ..
                        # helper lines
                        ax1.axvline(x=event.xdata,color="darkorange", linestyle="--")
                        ax3.axhline(y=event.xdata,color="darkorange", linestyle="--")
                    elif id(axi) == id(ax3) :
                        # helper text
                        # ..
                        # helper lines
                        ax0.axvline(x=event.xdata,color="darkorange", linestyle="--")
                        ax1.axvline(x=event.ydata,color="darkorange", linestyle="--")
                        ax2.axvline(x=event.ydata,color="darkorange", linestyle="--")
                    # plot crosshair
                    axi.axhline(y=event.ydata,color="orangered", linestyle="--")
                    axi.axvline(x=event.xdata,color="orangered", linestyle="--")
                    # draw
                    plt.draw()
            else:
                print(" > double click for crosshair view")
            return None
        cid = fig.canvas.mpl_connect('button_press_event', onclick)

        # .. end plotting or handover
        print(' > OUTPUT')
        check_path = root+os.path.sep+'TMP'
        export.check_isdir(check_path)
        plt.savefig(check_path+os.path.sep+'shotrun_overview.png')
        if not silent_run and matplotlib_canvas is None:
            print("   NEW INTERACTIVE SCREEN")
            plt.show()
        if matplotlib_canvas is not None and False:
            print("   ADD TO INTERACTIVE SCREEN")
            plt.draw()
        else:
            print("   EXIT")
            fig.canvas.mpl_disconnect(cre)
            fig.canvas.mpl_disconnect(cid)
            plt.close()
    
    print("END PROCESS DATA\n")    
    return explored
    
def explorador_batch(**kwargs): 
    # GLOBALS
    glob_dict = globals()
    global root
    
    # EXPECTED
    global runtime_vars
    
    # OPTIONS
    data_files = kwargs.get('data_files',None)
    silent_run = kwargs.get('silent_run',False)

    # RUN
    # clarify files
    if data_files is None:
        data_files = select_batch(m_GUI=True)
    # process files
    input_stream = []
    if isinstance(data_files,list) or isinstance(data_files,np.ndarray):
        try:
            for file in data_files:
                load_data = explorador_archivos(data_file=file,plotting=False,runtime_vars=runtime_vars)
                if isinstance(load_data,type(None)):
                    print('MISSING ERROR HANDLER: INVALID OUTPUT')
                    print(file)
                else:
                    input_stream.append(load_data)
        except:
            print("MISSING ERROR HANDLER: PROBLEM DURING WORK ON FILE LIST")
    else:
        try:
            input_stream.append(explorador_archivos(data_file=data_files,plotting=False,runtime_vars=runtime_vars))
        except:
            print("MISSING ERROR HANDLER: PROBLEM DURING WORK ON SINGLE FILE")
    
    # prepare batched dataset
    batched = {}
    batched['timedomain'] = []
    batched['signal_data'] = []
    
    batched['corrected_data'] = []
    
    batched['frequencydomain'] = []
    batched['FFT_amplitudes'] = []
    
    batched['is_set_filteredFFT'] = []
    batched['is_set_renorm'] = []
    
    batched['valid_frequencies'] = []
    batched['modified_frequencies'] = []
    
    batched['signal_cleaned_data'] = []
    batched['frequencydomain_cleaned'] = []
    batched['FFT_amplitudes_cleaned'] = []
    
    print("\nPROCESS BATCH")
    
    timedomain_is_always_the_same = True
    frequencydomain_is_always_the_same = True
    
    power_equivalent = []
    power_equivalent_of_average = None
    
    minmax_amplitude = []
    minmax_amplitude_of_average = None
    
    for dataset in input_stream:
        if isinstance(dataset['corrected_data'],type(None)) or isinstance(dataset['frequencydomain'],type(None)) or \
           isinstance(dataset['valid_frequencies'],type(None)) or isinstance(dataset['FFT_amplitudes'],type(None)):
            continue
        
        # stream the timebase
        if len(batched['timedomain']) > 0:
            elementiwise_coparision = np.equal(batched['timedomain'],dataset['timedomain'])
            if not np.any(elementiwise_coparision):
                print('    TIME BASE DISCREPANCY WITHIN DATASET')
                timedomain_is_always_the_same = False
        
        batched['timedomain'].append(dataset['timedomain'])
        
        # stream the signal data
        batched['corrected_data'].append(dataset['corrected_data'])
        
        # stream the frequency domain
        if len(batched['frequencydomain']) > 0:
            elementiwise_coparision = np.equal(batched['frequencydomain'],dataset['frequencydomain'])
            if not np.any(elementiwise_coparision):
                print('    FREQUENCY BASE DISCREPANCY WITHIN DATASET')
                frequencydomain_is_always_the_same = False

        batched['frequencydomain'].append(dataset['frequencydomain'])
        batched['valid_frequencies'].append(dataset['valid_frequencies'])
        
        # stream spectrum
        batched['FFT_amplitudes'].append(dataset['FFT_amplitudes'])

        # stream meta settings
        batched['is_set_renorm'].append(dataset['is_set_renorm'])
        batched['is_set_filteredFFT'].append(dataset['is_set_filteredFFT'])
        
        # stream cleaned signal
        if dataset['is_set_renorm']:
            batched['modified_frequencies'].append(dataset['modified_frequencies'])
            batched['signal_cleaned_data'].append(dataset['signal_cleaned_data'])
            batched['frequencydomain_cleaned'].append(dataset['frequencydomain_cleaned'])
            batched['FFT_amplitudes_cleaned'].append(dataset['FFT_amplitudes_cleaned'])
            
        # find more statistical infos about waveform
        if dataset['is_set_renorm']:
            # calculate power equivalent
            power_equivalent.append(np.trapz((dataset['signal_cleaned_data'])**2, x=dataset['timedomain']))
            # find minimum and maximum amplitudes
            minmax_amplitude.append([np.nanmin(dataset['signal_cleaned_data']),np.nanmax(dataset['signal_cleaned_data'])])
        else:
            # calculate power equivalent
            power_equivalent.append(np.trapz((dataset['corrected_data'])**2, x=dataset['timedomain']))
            # find minimum and maximum amplitudes
            minmax_amplitude.append([np.nanmin(dataset['corrected_data']),np.nanmax(dataset['corrected_data'])])
        
    # type transform
    minmax_amplitude = np.array(minmax_amplitude)
        
    # shrink payload
    if timedomain_is_always_the_same:
        batched['timedomain'] = batched['timedomain'][0]
    if frequencydomain_is_always_the_same:
        batched['frequencydomain'] = batched['frequencydomain'][0]
        batched['valid_frequencies'] = batched['valid_frequencies'][0]
        batched['modified_frequencies'] = batched['modified_frequencies'][0]
    
    # calculate average spectrum
    if frequencydomain_is_always_the_same:
        if np.any(np.array(batched['is_set_renorm'])):
            batched['amplitude_spectrum'] = np.abs(batched['FFT_amplitudes_cleaned'])
            batched['phase_spectrum']     = np.angle(batched['FFT_amplitudes_cleaned'])
        else:
            batched['amplitude_spectrum'] = np.abs(batched['FFT_amplitudes'])
            batched['phase_spectrum']     = np.angle(batched['FFT_amplitudes'])
        
        batched['average_amplitude'] = np.nanmean(batched['amplitude_spectrum'], axis=0)
        batched['average_phase'] = np.nanmean(batched['phase_spectrum'], axis=0)
        batched['minimum_amplitude'] = np.nanmin(batched['amplitude_spectrum'], axis=0)
        batched['maximum_amplitude'] = np.nanmax(batched['amplitude_spectrum'], axis=0)
        batched['minimum_phase'] = np.nanmin(batched['phase_spectrum'], axis=0)
        batched['maximum_phase'] = np.nanmax(batched['phase_spectrum'], axis=0)
        
        if timedomain_is_always_the_same:
            # get back to complex numbers
            # batched['average_spectrum'] = batched['average_amplitude'] * np.exp(1j*batched['average_phase']) # gives beats
            if np.any(np.array(batched['is_set_renorm'])):
                batched['average_spectrum'] = np.nanmean(batched['FFT_amplitudes_cleaned'], axis=0)
            else:
                batched['average_spectrum'] = np.nanmean(batched['FFT_amplitudes'], axis=0)
            
            # inverse FFT to obtain average waveform
            print('   RE-TRANSFORM AVERAGED SIGNAL..')
            print('   ..DEPLOY LENGTH OF FIRST TIMETRACE IN DATA STACK')
            batched['average_signal'] = scipy.fft.irfft(batched['average_spectrum'], len(batched['timedomain']))
    
            # calculate power of average waveform
            power_equivalent_of_average = np.trapz((batched['average_signal'])**2, x=batched['timedomain'])    
            
            # find minimum and maximum amplitudes
            minmax_amplitude_of_average = np.array([np.nanmin(batched['average_signal']),np.nanmax(batched['average_signal'])])
            batched['minmax_amplitude_of_average'] = minmax_amplitude_of_average
    
    # write out
    print(' > WRITE OUTPUT')
    check_path = root+os.path.sep+'TMP'
    export.check_isdir(check_path)
    export_to = check_path+os.path.sep+'shotrun_overview'
    try:
        if os.path.exists(export_to):
            os.remove(export_to)
        np.save(export_to,batched)
        np.savetxt(export_to+"_max.dat",minmax_amplitude[:,1])
        np.savetxt(export_to+"_min.dat",minmax_amplitude[:,0])
        print("   EXPORTED DATA TO\n   "+export_to)
    except:
        print("   MISSING ERROR HANDLER: PROBLEM DURING DATA WRITE OUT TO\n   "+export_to)
    
    # plotting
    if not silent_run:
        # .. prepare canvas
        plt.ioff()
        fig = plt.figure(figsize=(15,10), dpi=100)
        gs = fig.add_gridspec(2,2) #(3,2)
        ax0 = fig.add_subplot(gs[1, 0])
        ax3 = fig.add_subplot(gs[0, 0])
        ax1 = fig.add_subplot(gs[0, 1])
        ax1r = ax1.twinx()
        ax2 = fig.add_subplot(gs[1, 1])
        ax2r = ax2.twinx()
        print('   CANVAS OK')
        # .. helper functions
        #    https://stackoverflow.com/a/14314054
        def moving_average(a, n=3) :
            ret = np.cumsum(a, dtype=float)
            ret[n:] = ret[n:] - ret[:-n]
            return ret[n - 1:] / n
        # .. plot spectrum averaged reconstructed signal
        def plot_ax0():
            nonlocal ax0
            nonlocal power_equivalent
            nonlocal power_equivalent_of_average
            # select case
            if 'average_signal' in batched.keys():
                # parse label
                plotlabel = "Recontructed Average"
                if np.any(np.array(batched['is_set_filteredFFT'])):
                    plotlabel = plotlabel+':\nbandwidth limited'
                if np.any(np.array(batched['is_set_renorm'])):
                    plotlabel = plotlabel + "\n(w-domain corrected)"
                # plot
                ax0.plot(const.s_to_ns*batched['timedomain'],batched['average_signal'],label=plotlabel, c='green')
            else:
                for n in range(len(batched)):
                    if batched['is_set_renorm'][n]:
                        if timedomain_is_always_the_same:
                            ax0.plot(const.s_to_ns*batched['timedomain'],batched['signal_cleaned_data'][n])
                        else:
                            ax0.plot(const.s_to_ns*batched['timedomain'][n],batched['signal_cleaned_data'][n])
                    else:
                        if timedomain_is_always_the_same:
                            ax0.plot(const.s_to_ns*batched['timedomain'],batched['corrected_data'][n])
                        else:
                            ax0.plot(const.s_to_ns*batched['timedomain'][n],batched['corrected_data'][n])
            # legend
            ax0.legend()
            ax0.set_title('Signal')
            ax0.set_xlabel('Time (ns)')
            if 'quantity' in runtime_vars.keys():
                ax0.set_ylabel(runtime_vars['quantity'] + ' ('+antennas.symbols[runtime_vars['quantity']]['label']+')')
            else:
                ax0.set_ylabel('Amplitude (V)')
        print('   Plot Measurement or Reconstructed Averaged Signal')
        plot_ax0()
        
        # .. averaged spectrum
        def plot_ax1():
            nonlocal ax1
            nonlocal ax1r
            averaging = 10
            stub = averaging % 2
            offs = int((averaging - stub)/2)
            if 'average_amplitude' in batched.keys():
                # parse label
                plotlabel = "Amplitude"
                if np.any(np.array(batched['is_set_filteredFFT'])):
                    plotlabel = plotlabel+':\nbandwidth limited'
                if np.any(np.array(batched['is_set_renorm'])):
                    plotlabel = plotlabel + "\n(w-domain corrected)"
                # plot
                if np.any(np.array(batched['is_set_renorm'])):
                    # range of phase values
                    ax1r.fill_between(batched['frequencydomain'][batched['modified_frequencies']],batched['minimum_phase'][batched['modified_frequencies']],batched['maximum_phase'][batched['modified_frequencies']],label='Phase range',color='c',hatch='/',alpha=0.05)
                    # full average
                    ax1.scatter(batched['frequencydomain'][batched['modified_frequencies']],batched['average_amplitude'][batched['modified_frequencies']],label=plotlabel, c='green')
                    # moving average on average
                    ax1.plot(batched['frequencydomain'][batched['modified_frequencies']][offs:-offs+1],moving_average(batched['average_amplitude'][batched['modified_frequencies']],2*offs), color='green')
                    # range of amplitude values
                    ax1.fill_between(batched['frequencydomain'][batched['modified_frequencies']],batched['minimum_amplitude'][batched['modified_frequencies']],batched['maximum_amplitude'][batched['modified_frequencies']],label='Amplitude range',color='red',alpha=0.2)

                else:
                    # range of phase values
                    ax1r.fill_between(batched['frequencydomain'],batched['minimum_phase'],batched['maximum_phase'],label='Phase range',color='c',hatch='/',alpha=0.05)
                    # full average
                    ax1.scatter(batched['frequencydomain'],batched['average_amplitude'],label=plotlabel,color='blue')
                    # moving average on average
                    ax1.plot(batched['frequencydomain'][offs:-offs+1],moving_average(batched['average_amplitude'],2*offs), color='blue')
                    # range of values
                    ax1.fill_between(batched['frequencydomain'],batched['minimum_amplitude'],batched['maximum_amplitude'],label='Amplitude range',color='red',alpha=0.2)
            # legend
            ax1.legend() # TODO add ax1r legend
            ax1.set_title('Average Spectrum')
            ax1.set_xlabel('Frequency (Hz)')
            ax1.set_ylabel('Amplitude (Vs)')
            ax1r.set_ylabel('Phase (rad)')
            fig.tight_layout()
        print('   SPECTRUM')
        plot_ax1()
        
        # .. plot power equivalent
        def plot_ax2():
            nonlocal ax2
            nonlocal power_equivalent
            nonlocal minmax_amplitude
            # power equivalent
            lns1 = ax2.plot(range(len(power_equivalent)),power_equivalent,label='power equivalent (PE)',c='blue')
            lns3 = ax2.axhline(y = np.mean(np.array(power_equivalent)),label='mean PE', c='blue', linestyle='--')
            if power_equivalent_of_average != None:
                lns5 = ax2.axhline(y = power_equivalent_of_average,label='PE of reconstructed average', c='blue', linestyle=':')
            # minimum amplitude
            lns2 = ax2r.plot(range(len(minmax_amplitude[:,0])),minmax_amplitude[:,0],label='absolute minimum amplitude (aMinA)',c='red')
            lns4 = ax2r.axhline(y = np.mean(np.array(minmax_amplitude[:,0])),label='mean aMinA', c='red', linestyle='--')
            if isinstance(minmax_amplitude_of_average,np.ndarray):
                lns6 = ax2r.axhline(y = minmax_amplitude_of_average[0],label='aMinA of reconstructed average', c='red', linestyle=':')
            # maximum amplitude
            lns101 = ax2r.plot(range(len(minmax_amplitude[:,1])),minmax_amplitude[:,1],label='absolute maximum amplitude (aMaxA)',c='green')
            lns103 = ax2r.axhline(y = np.mean(np.array(minmax_amplitude[:,1])),label='mean aMaxA', c='green', linestyle='--')
            if isinstance(minmax_amplitude_of_average,np.ndarray):
                lns105 = ax2r.axhline(y = minmax_amplitude_of_average[1],label='aMaxA of reconstructed average', c='green', linestyle=':')
            # build legend # TODO add also labels of 2Dline ? 'TypeError: can only concatenate list (not "Line2D") to list'
            if isinstance(minmax_amplitude_of_average,np.ndarray):
                lns = lns1 + lns2 + lns101
            else:
                lns = lns1 + lns2 + lns101
            labs = [l.get_label() for l in lns]
            ax2.legend(lns,labs)
            # labels
            ax2.set_title('Trends')
            ax2.set_xlabel('Dataset #')
            ax2.set_ylabel('Power (arb.u.)')
            ax2r.set_ylabel('Amplitude (arb.u.)')
        print('   Power Equivalent')
        plot_ax2()
        
        # .. style event management
        plt.tight_layout()
        def resize_here():
            plt.tight_layout()
        cre = fig.canvas.mpl_connect('resize_event', resize_here)
        
        # .. mouse klick event management
        coordinates = None
        def onclick(event):
            nonlocal coordinates
            nonlocal ax0
            nonlocal ax1
            nonlocal ax2
            nonlocal ax3
            if event.dblclick:
                # feedback
                coordinates = event.xdata, event.ydata
                print(" > click coordinates "+str(coordinates))
                # plotting
                if event.inaxes is not None:
                    # get position
                    axi = event.inaxes
                    # clear and replot
                    ax0.clear()
                    plot_ax0()
                    ax1.clear()
                    plot_ax1()
                    ax2.clear()
                    plot_ax2()
                    ax3.clear()
                    plot_ax3()
                    # auxiliary indication
                    if id(axi) == id(ax0) :
                        # helper text
                        # ..
                        # helper lines
                        ax3.axvline(x=event.xdata,color="darkorange", linestyle="--")
                    elif id(axi) == id(ax1) :
                        # helper text
                        # ..
                        # helper lines
                        ax2.axvline(x=event.xdata,color="darkorange", linestyle="--")
                        ax3.axhline(y=event.xdata,color="darkorange", linestyle="--")
                    elif id(axi) == id(ax2) :
                        # helper text
                        # ..
                        # helper lines
                        ax1.axvline(x=event.xdata,color="darkorange", linestyle="--")
                        ax3.axhline(y=event.xdata,color="darkorange", linestyle="--")
                    elif id(axi) == id(ax3) :
                        # helper text
                        # ..
                        # helper lines
                        ax0.axvline(x=event.xdata,color="darkorange", linestyle="--")
                        ax1.axvline(x=event.ydata,color="darkorange", linestyle="--")
                        ax2.axvline(x=event.ydata,color="darkorange", linestyle="--")
                    # plot crosshair
                    axi.axhline(y=event.ydata,color="orangered", linestyle="--")
                    axi.axvline(x=event.xdata,color="orangered", linestyle="--")
                    # draw
                    plt.draw()
            else:
                print(" > double click for crosshair view")
            return None
        cid = fig.canvas.mpl_connect('button_press_event', onclick)
        # .. end plotting or handover
        print(' > OUTPUT')
        check_path = root+os.path.sep+'TMP'
        export.check_isdir(check_path)
        export_to = check_path+os.path.sep+'shotrun_overview.png'
        try:
            if os.path.exists(export_to):
                os.remove(export_to)
            plt.savefig(export_to)
            print("   EXPORTED PLOT TO\n   "+export_to)
        except:
            print("   MISSING ERROR HANDLER: PROBLEM DURING PLOT WRITE OUT\n   "+export_to)
        if not silent_run:
            print("   NEW INTERACTIVE SCREEN")
            plt.show()
        else:
            print("   EXIT")
            fig.canvas.mpl_disconnect(cre)
            fig.canvas.mpl_disconnect(cid)
            plt.close()
    
    # WRITE OUT
    return True

# MAIN MENUE ITEMS AND MAIN WINDOW
def run_GUI():
    # GLOBALS
    global runtime_vars
    # Create the root window 
    window = Tk() 

    # Set window title
    window.title("MAIN MENU")
       
    # set window size via trim("int(width) x int(height) + int(xposition) + int(yposition)")
    wxh = func.screensize()
    screen_width = int(wxh.split('x')[0])
    screen_height = int(wxh.split('x')[1])
    scale = 3
    geometry_string = str(int(screen_width/scale))+"x"+str(int(screen_height/scale))+\
                       "+"+str(int(screen_width/3))+"+"+str(int(screen_height/3))
    window.geometry(geometry_string)
       
    #Set window background color 
    window.config(background = "black") 
       
    # Create a File Explorer label 
    label_file_explorer = Label(window,  
                                text = "WAVEFORM ANALYZER", 
                                width = 50, height = 4,  
                                fg = "lightgreen",
                                background="black") 


    #create buttons and labels
    # menue items for options
    def goto_settings():
        window.destroy()
        interactive_input(exit_call = run_GUI, runtime_vars = runtime_vars)
        return True
    
    button_goto_settings =   Button(\
                               window,  \
                               text = "SETTINGS", \
                               command = goto_settings\
                             )
    
    # high level actions triggered by buttons are only possible after a reference is processed
    #  thus the trigger buttons are disabled by default
    button_explore=Button(window,  
                          text = "BROWSE FILE",
                          state=tk.DISABLED,
                          command = lambda: explorador_archivos(runtime_vars = runtime_vars))
    button_batch  =Button(window,  
                          text = "BROWSE BATCH",
                          state=tk.DISABLED,
                          command = lambda: explorador_batch(runtime_vars = runtime_vars)) 
    
    # enable functionalities functions
    def unblocking(**kwargs):
        nonlocal button_explore
        nonlocal button_batch
        # deblock functionalities
        print(' > unblock further options')
        button_explore['state']  = tk.NORMAL
        button_batch['state'] = tk.NORMAL
        return True
            
    def callback_reference(**kwargs):
        runtime_vars = kwargs.get('runtime_vars',{})
        print('\nCALLBACK REFERENCE')
        print(' > browse reference')
        exit_status = explorador_referencia(runtime_vars=runtime_vars)
        if exit_status:
            print(' > unblock buttons')
            unblocking()
        else:
            print(' > no reference parsed to memory')
        print('EXIT CALLBACK REFERENCE\n')
        return True
        
    # the reference button and primitive buttons are enabled from the beginning  
    button_reference=Button(window,  
                          text = "BROWSE REFERENCE", 
                          command = lambda: callback_reference(runtime_vars=runtime_vars))
    button_save=Button(window, 
                          text="SAVE AND CONTINUE",
                          command=lambda: print("TODO"))

    # labels
    label_file_explorer.grid(column = 1, row = 1, columnspan = 3) 

    button_reference.grid(column = 2, row = 3, sticky="news") 
    button_explore.grid(column = 2, row = 4, sticky="news")
    button_batch.grid(column = 2, row = 5, sticky="news")

    button_goto_settings.grid(column=3, row=7)
    button_save.grid(column=3, row=8)


    # Let the window wait for any events 
    window.mainloop() 

# RUN WITH MODIFIED RUNTIME PARAMETERS
if globals()["__name__"] == '__main__':
    # STARTUP
    if input_dict['GUI'] == 'T' :
        # interactive modifications
        print("\nGUI STARTUP")
        # call interactive start window
        interactive_start() 
        # main GUI
        run_GUI()
    else:
        func.MESSAGE_PRINT(glob_dict['__file__'],"There is no alternative to a GUI run currently. \nSet the parameter GUI in the input file to T and \nrelaunch the application.")


# ==============================================================================
# TERMINATE
# ==============================================================================
# TAKE TIME
fin = time.time()

# PROMPT RUNTIME
print("\nRUNTIME = %3.1f s" % (fin-debut))

# SAY GOODBYE
func.GOODBYE()