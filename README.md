**Guide to installation and first use**

# Runtime Environment

The python script is best cloned from the source (not downloaded) and executed within an Anaconda environment (not within your standard python environment). The first is usefull for uncomplicated updates and an easy contribution to the project, the later is ought to protect your habitual working environments and keep them clean. The installation and execution procedures are kept simple in order to allow users to analyze data who are not familiar to python.

## GIT Environment

*pyWA* is a collaborative project, we would like to encourage that you get started with [git](https://git-scm.com/downloads) in order to join the developpers. Anyway, please use to following method to install the relevant scripts on your computer:

1. make sure git is installed
  A. in Windows search for `gitbash` and install [git for windows](https://gitforwindows.org/) if you can not locate it,
  B. in POSIX systems try `which git` and install [git](https://git-scm.com/downloads) if no answer is given to you; 
2. copy the project files from the internet onto your computer
  1. open *gitbash* or *terminal* respectively,
  2. navigate to a location that you want to use as working directory - e.g. in Windows `C:\working\directory` or in POSIX sustems `/working/directory`,
  3. type `git clone https://srvgitlab.clpu.int/mehret/pyWA.git` and download the project files by executing with `enter` - here `https://srvgitlab.clpu.int/mehret/pyWA.git` is the default repository location which you can change if your institution has another version of the code on some server - or if you have a version of the code in a file;
3. if you open a filemanager, you can take a look in the created directory `pyWA` - it should be populated with some files and directories.

## Python Environment

We recommend you to set up a clean python environment with [Anaconda](https://www.anaconda.com/distribution/). Set up Anaconda (version 3) by following the installation guides on the project's web page. If you do not like to work with Anaconda, please find the details of used packages in `pywa/pywa.yml`. Depending on the operating system, the next setup steps differ.

### Windows
Open Anaconda navigator, navigate to *Environments* and click on the play button behind *base* to launch the anaconda prompt. Navigate to the `pyWA` directory, then execute the following installation instructions (answer with yes, if asked for permission to install packages)
```
(base) C:\working\directory\pyWA> conda env create -f WA.yml
(base) C:\working\directory\pyWA> conda activate WA
(WA) C:\working\directory\pyWA> pip install LIB\RTxReadBin-1.0-py3-none-any.whl --no-deps
```

### Linux
```
(base) user@computer:/working/directory/pyWA$ conda env create -f WA.yml
(base) user@computer:/working/directory/pyWA$ conda activate WA
(WA) user@computer:/working/directory/pyWA$ pip install LIB\RTxReadBin-1.0-py3-none-any.whl --no-deps
```

## Test Run

Perform a first run as test to ensure that the environment works fine. Execute
```
python run.py
```

## Known Errors and Troubleshooting

### OpenCV

Often the module *CV2* is not recognized, manifested with the error
```
    import cv2
ImportError: DLL load failed: The specified module could not be found.
```
then do
```
conda update -n base -c defaults conda
conda install -c fastai opencv-python-headless
conda uninstall opencv
conda uninstall opencv-python-headless
y
```
which shakes a bit the faulty installation and eventually brings you CV2 with GUI support.

---

# Workflow

Run via
```
python run.py
```

---

# Milestones

- &check; setup project repository with first version
- &cross; create user dialogue for cropping of data and definition for frequency range of interest
- &cross; workover GUI
- &cross; design test run scenario
