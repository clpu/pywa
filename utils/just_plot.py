# -*- coding: utf-8 -*-
"""
Run this script on a waveform file and get the channels plotted.

python just_plot.py -h

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
import os
import sys
import time

import signal
import readchar

import numpy as np

import matplotlib.pyplot as plt

# =============================================================================
# RUNTIME
# =============================================================================
# MANAGER
def DECOMPOSE_FILENAME(filename):
    name, extension = os.path.splitext(filename)
    path = os.path.dirname(filename)
    name = name.replace(path+os.path.sep,"")
    return {"path":path, "name":name, "extension":extension}
    
def CATCH(signum, frame):
    msg = "Ctrl-c was pressed. Do you really want to exit? y/n "
    print(msg, end="", flush=True)
    res = readchar.readchar()
    if res == b'y':
        eval("statistical_writeout()")
        print("")
        exit(1)
    else:
        print("", end="\r", flush=True)
        print(" " * len(msg), end="", flush=True) # clear the printed line
        print("    ", end="\r", flush=True)

# PROMPT GREETINGS
print('\nWELCOME TO pyWA UTILITIES - JUST PLOT')

# LIBRARIES
run_lib = {}

# RUNTIME LIBRARY
run_lib = {'debut' : time.time()}

# SET PARAMETERS
# plot colors
colors = ['orange','green','blue','red','black','cyan','yellow']

# ROOM FOR TESTS
# ..

# =============================================================================
# STARTUP
# =============================================================================
# SIGNAL INTERCEPTION
signal.signal(signal.SIGINT, CATCH)

# COMMAND
import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-f','--filename', nargs='+', type=str, help='Filename of waveform data with channels in columns and time in rows.')

parser.add_argument('-pw','--plotwfm', nargs='?', type=str, default='Y', help='Plot individual waveform data, pass Y for Yes and N for No.')

parser.add_argument('-sr','--statrun', nargs='?', type=str, default='Y', help='Evaluate statistics about parsed waveform data, pass Y for Yes and N for No.')

run_lib['command'] = parser.parse_args()

del argparse, parser

# CLEAN INPUT STRINGS AND RESOLVE WILDCARDS
from glob import glob

replace = []

for i in range(len(run_lib['command'].filename)):
    run_lib['command'].filename[i] = run_lib['command'].filename[i].strip("'").strip('"')
    resolved = glob(run_lib['command'].filename[i])
    if len(resolved) > 1:
        replace.append(run_lib['command'].filename[i])
        for item in resolved:
            run_lib['command'].filename.append(item.replace('\\','\\\\').replace('\\\\\\\\','\\\\')) # backlashes treated due to bug in glob

for file in run_lib['command'].filename:
    file_intelligence = DECOMPOSE_FILENAME(file)
    # get rid of R&S extra files
    if os.path.exists(file_intelligence["path"]+os.path.sep+file_intelligence["name"]+".Wfm"+file_intelligence["extension"]):
        replace.append(file)
    
for item in replace:
    run_lib['command'].filename.remove(item)

del glob,resolved,replace,file_intelligence

# ==============================================================================
# JOB MANAGER
# ==============================================================================
# SORT OUT TASKS
job_lst = []

if len(run_lib['command'].filename) == 1:
    if run_lib['command'].plotwfm == "Y":
        job_lst.append("individual_analysis('"+run_lib['command'].filename[0]+"')")
    if run_lib['command'].statrun == "Y":
        job_lst.append("statistical_analysis('"+run_lib['command'].filename[0]+"')")
else:
    for filename in run_lib['command'].filename:
        if run_lib['command'].plotwfm == "Y":
            job_lst.append("individual_analysis('"+filename+"')")
        if run_lib['command'].statrun == "Y":
            job_lst.append("statistical_analysis('"+filename+"')")

if run_lib['command'].statrun == "Y":
    job_lst.append("statistical_writeout()")

# =============================================================================
# MAIN FUNCTIONS
# =============================================================================
# LOAD DATA
def load_data(from_file):
    # sort out exceptions
    file_intelligence = DECOMPOSE_FILENAME(from_file)
    if file_intelligence['extension'] == ".bin":
        from RSRTxReadBin import RTxReadBin
        # load all
        y, x, S = RTxReadBin(from_file)
        vertical = np.transpose(y[:,0,:]) 
        #print(S['MultiChannelVerticalOffset'])
        horizontal = x
    else:
        # find data
        skiplines = 0
        file = open(from_file, 'r')
        lines = file.readlines()

        for number,line in enumerate(lines):
            try:
                first_character = line.strip()[0]
                if (first_character.isnumeric() or first_character == "-"):
                    skiplines = number
                    break
            except:
                continue
        # load data
        try:
            contents = np.loadtxt(from_file,skiprows=skiplines)
        except:
            contents = np.genfromtxt(from_file,skip_header=skiplines,delimiter=",",filling_values=np.NaN)
            contents = contents[:, ~np.isnan(contents).any(axis=0)]
        # try to find timebase
        if all(np.isclose(np.diff(contents[:,0]),np.diff(contents[:,0])[0])):
            horizontal = contents[:,0]
            vertical = np.transpose(contents[:,1:])
        else:
            vertical = np.transpose(contents)
            # known exception: R&S
            timebase_known = False
            if ".Wfm." in from_file or ".wfm." in from_file:
                meta_file = from_file.lower().replace(".wfm.",".")
                # known keywords
                file = open(meta_file, 'r')
                lines = file.readlines()
                for line in lines:
                    if "resolution" in line.lower():
                        try:
                            find_increment = line.lower().replace("resolution","").strip().strip(":;,")
                            horizontal = np.arange(vertical.shape[1]) * np.float(find_increment)
                            timebase_known = True
                            break
                        except:
                            continue
            # manual input
            if not timebase_known:
                ask_increment = input("Please enter the increment of timesteps:\n")
                horizontal = np.arange(vertical.shape[1]) * np.float(ask_increment)
    # return
    return {"horizontal":horizontal, "vertical":vertical}

# ==============================================================================
# MAIN FUNCIONALITIES
# ==============================================================================
# FILE ANALYSIS
def individual_analysis(filename):
    # load horizontal and vertical axis
    data = load_data(filename)
    # print extreme amplitudes
    # ...
    # plot waveforms  
    fig, ax = plt.subplots()
    fig.subplots_adjust(right=0.75)
    
    plots = []
    axes = []
    spacing = 0.05
    for i in range(data['vertical'].shape[0]):
        if i > 0:
            axes.append(ax.twinx())
            axes[i].spines.right.set_position(("axes", 1.+i*spacing))
        else:
            axes.append(ax)
        plots.append(axes[i].plot(data['horizontal'],data['vertical'][i,:],c=colors[i], label="Wfm "+str(i+1)))
        
    plot_lines = []
    for p in plots:
        plot_lines.append(p[0])
    
    tkw = dict(size=4, width=1.5)
    for i,p in enumerate(plot_lines):
        axes[i].yaxis.label.set_color(p.get_color())
        axes[i].tick_params(axis='y', colors=p.get_color(), **tkw)
    
    ax.tick_params(axis='x', **tkw)
        
    ax.legend(handles=plot_lines)
    
    plt.title(filename)
    
    plt.tight_layout()
    
    plt.show()
    
    return True

def statistical_analysis(filename):
    global run_lib
    # load horizontal and vertical axis
    data = load_data(filename)
    # prepare output
    if "statistics" not in run_lib.keys():
        run_lib['statistics'] = {}
        run_lib['statistics']['maxima'] = {'values':np.zeros(data['vertical'].shape[0]), 'locations':np.chararray(data['vertical'].shape[0], itemsize=2048, unicode = True)}
        run_lib['statistics']['minima'] = {'values':np.zeros(data['vertical'].shape[0]), 'locations':np.chararray(data['vertical'].shape[0], itemsize=2048, unicode = True)}
    # find maxima
    maxima = np.nanmax(data['vertical'], axis = 1)
    minima = np.nanmin(data['vertical'], axis = 1)
    maxima = np.where(maxima==np.inf,0.,maxima)
    minima = np.where(minima==-np.inf,0.,minima)
    # compare with stored values
    new_maximum = np.greater_equal(maxima,run_lib['statistics']['maxima']['values'])
    new_minimum = np.less_equal(minima,run_lib['statistics']['minima']['values'])
    run_lib['statistics']['maxima']['values'] = np.where(new_maximum,maxima,run_lib['statistics']['maxima']['values'])
    run_lib['statistics']['minima']['values'] = np.where(new_minimum,minima,run_lib['statistics']['minima']['values'])
    run_lib['statistics']['maxima']['locations'] = np.where(new_maximum,filename,run_lib['statistics']['maxima']['locations'])
    run_lib['statistics']['minima']['locations'] = np.where(new_minimum,filename,run_lib['statistics']['minima']['locations'])
    
    return True
    
def statistical_writeout():
    global run_lib
    # print results
    print("\nSTATISTICS")
    for i in range(run_lib['statistics']['maxima']['values'].shape[0]):
       print(">Wfm"+str(i+1))
       print(" maximum = "+str(run_lib['statistics']['maxima']['values'][i]))
       print(" - in "+run_lib['statistics']['maxima']['locations'][i])
       print(" minimum = "+str(run_lib['statistics']['minima']['values'][i]))
       print(" - in "+run_lib['statistics']['minima']['locations'][i])
       
    return True
    
# ==============================================================================
# RUN
# ==============================================================================
for job in job_lst:
    exit_stat = eval(job)
    if exit_stat != True:
        print(job+"\n>terminates with "+str(exit_stat))

# ==============================================================================
# TERMINATE
# ==============================================================================
# TAKE TIME
run_lib['fin'] = time.time()

print(globals()['__file__']+"\n>runtime was {} s".format(run_lib['fin']-run_lib['debut']))

# SAVE RUNTIME LIBRARY
# ..

# EXIT
print('\nEND OF SCRIPT')